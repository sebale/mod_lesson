<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Assign quiz to lesson. Html form
 *
 * @package mod_lesson
 * @copyright  2016 InveritaSoft
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/

require_once('../../config.php');
require_once($CFG->dirroot.'/mod/lesson/locallib.php');
require_once($CFG->dirroot.'/mod/lesson/quiz_assign_form.php');

$id = required_param('id', PARAM_INT);

$cm = get_coursemodule_from_id('lesson', $id, 0, false, MUST_EXIST);
$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
$lesson = new lesson($DB->get_record('lesson', array('id' => $cm->instance), '*', MUST_EXIST));

require_login($course, false, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/lesson:manage', $context);

$url = new moodle_url('/mod/lesson/quiz_assign.php', array('id'=>$id));
$url->param('id', $id);

$PAGE->set_url($url);

$lessonoutput = $PAGE->get_renderer('mod_lesson');

$currenttab = 'quizassign';
echo $lessonoutput->header($lesson, $cm, $currenttab, false, null, get_string('quiz-assignment', 'lesson'));

$mform = new quiz_assign_form($url);

$quizes_records = $DB->get_records_select('quiz', 'course=' . $course->id, null, '', 'id, name');
$quizes = [];
$quizes[0] = get_string('none', 'lesson');

foreach ($quizes_records as $quiz) {
	$quizes[$quiz->id] = $quiz->name;
}

$mform->quizes = $quizes;

$mform->getQuizSelect('quiz', $lesson->quiz);
$mform->addActionButtons();


//Form processing and displaying is done here
if ($mform->is_cancelled()) {
	redirect($url);
	exit;
} else if ($fromform = $mform->get_data()) {
	$data = new stdClass;
	$data->id = $lesson->id;
	$data->quiz = $fromform->quiz;
	$DB->update_record('lesson', $data);
}

$data = new stdClass;
$data->id = $PAGE->cm->id;

$mform->set_data($data);
$mform->display();

echo $lessonoutput->footer();